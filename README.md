# upload-artifact-registry

upload-artifact-registry is the GitLab component to copy Docker or [OCI](https://opencontainers.org/) images from the GitLab Container Registry to the [Google Artifact Registry](https://cloud.google.com/artifact-registry).

## Prerequisites

* A GitLab project with a Container Registry containing Docker images, the Component could follow a build image job to push image to GitLab Container Registry.
* A Google Cloud project with a configured Artifact Registry repository to host image.

## Usage

Include the upload-artifact-registry component in your `.gitlab-ci.yml` file:

```yaml
include:
  - component: gitlab.com/yongxuanzhang/ar-upload/ar-upload@a2528a0f89026591e457e46ff5212b8383d63499
    inputs:
      stage: deploy
      workload_identity_provider: "//iam.googleapis.com/projects/<project-number>/locations/global/workloadIdentityPools/<pool-id>/providers/<provider-id>"
      audience: "//iam.googleapis.com/projects/<project-number>/locations/global/workloadIdentityPools/<pool-id>/providers/<provider-id>"
      source: registry.gitlab.com/<group>/<project>/<source-image:tag>
      target: <location>-docker.pkg.dev/<project>/<repo>/<target-image:tag>
```


## Inputs

### Authentication

#### [Service Account Key JSON][sa-private-key]
-   `credentials_json_env_var`: (Optional) The env var containing the full credentials json (without `$`). The credentials json will be used to authenticate to Google Cloud services if provided. Follow the [instructions][instructions] to setup GitLab CI/CD env var.

#### [Workload Identity Federation][wif]
-   `workload_identity_provider`: (Optional) The full identifier of the Workload Identity Provider, including the project number, pool name, and provider name. This must be the full identifier which includes all parts:

    ``` yaml
        //iam.googleapis.com/projects/<project-number>/locations/global/workloadIdentityPools/<pool-id>/providers/<provider-id>
    ```

-   `service_account` (Optional): Email address or unique identifier of the Google Cloud Service Account for which to impersonate and generate credentials if provided. The Component attempts to authenticate through direct Workload Identity Federation otherwise.

-   `audience` (Optional): The value for the audience (aud) parameter in the generated GitLab Component OIDC token.

### Upload to Artifact Registry

- `as`(Optional default to `upload-artiface-registry`): The name for the job.

- `stage` (Optional default to `deploy`): The GitLab CI/CD stage where the component will be executed (defaults to 'deploy').

- `source` (Required): The full path to the source Docker image in your GitLab Container Registry (e.g., registry.gitlab.com/your-group/your-project/image-name:tag).

- `target` (Required): The desired name and tag for the copied image in the Google Artifact Registry. (e.g., LOCATION-docker.pkg.dev/your-gcp-project/your-repository/image-name:tag)

## Authorization

To use the Component, the provided Service Account/Workload Identity should have the following minimum roles:
  - Artifact Registry Writer (`roles/artifactregistry.writer`)
